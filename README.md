[![Moleculer](https://badgen.net/badge/Powered%20by/Moleculer/0e83cd)](https://moleculer.services)

# Exchanges parsing/collecting and currency converting service

Service runs with one rates source (Central Bank of Russia) and entirely based on USD currency mediator, which means that it stores all currencies rates related to USD.

## API

### Converting currencies

URL: **/converter/rates**

Method: **GET**

Params: 
* **from** __(required)__- source currency char code
* **to** _(optional)_ - destination currency char code
* **sourceId** _(optional)_ - source ID (ObjectID)

## Adapters

Adapters are made to convert sources data to JSON object with key-values like this:

```
{
    AZN: 0.5938,
    GBP: 1.2656,
    AMD: 0.002,
    BYN: 0.4844,
    BGN: 0.5625,
    BRL: 0.2344,
    ...
}
```

where value is a rate of current currency related to USD

At the moment there are only one exchange source adapter for Central Bank of Russia: ```/lib/Sources/adapters/cbr.adapter.js```

It extends **SourceAdapter** class.

## USAGE

API is available on *3000* port

_GET request example_: http://localhost:3000/api/converter/rates?from=USD&to=RUB

## TODO:

* validate source response with AJV (JSON Schema) in __SourceAdapter__ _validate()_ class method
* use numeric IDs for "Sources" instead of ObjectIds
* adding new sources (related adapters required)
* cache rules for converting and source currencies requests
* storing API credentials in Vault, fetching by "adapterName" key and put in HTTPClient on init

## Running in development mode

1. Install all the dependencies with ```npm install```

2. Run stack with docker-compose:

```
docker-compose -f dev.docker-compose.yml up --build
```

In detached mode:

```
docker-compose -f dev.docker-compose.yml up --build -d
```

## Running in production mode

```
docker-compose up --build
```

In detached mode:

```
docker-compose up --build -d
```