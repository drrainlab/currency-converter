const DbService	= require("moleculer-db");

module.exports = function(collection) {
	if (process.env.DB_URL) {
		const MongoAdapter = require("moleculer-db-adapter-mongo");
		return {
			mixins: [DbService],
			adapter: new MongoAdapter(`mongodb://${process.env.DB_URL}`, {
                keepAlive: true,
				useNewUrlParser: true
            }),
			collection
		};
	}
	return {
		mixins: []
	}
}