const axios = require('axios')
const XMLParser = require('fast-xml-parser')

const defaultConfig = { timeout: 4000 }

class HTTPClient {

    /**
     * HTTPClient constructor
     * @param {Object} httpClientConfig - HTTP client configuration
     * @returns new HTTPClient instance
     */

    constructor (httpClientConfig = {}) {
        this.http = axios.create(Object.assign({}, defaultConfig, httpClientConfig))
    }

    /**
     * GET request implementation
     * @param {String} url - URL
     * @param {Object} params - GET request params
     * @returns {Object} JSON representation of rates
     */  

    get (url, params) {
        console.log('Performing GET request: ', url, params);        
        return new Promise((resolve, reject) => {
            this.http.get(url, params)
                .then(response => {
                    let { data, headers } = response
                    if (headers['content-type'].match('json')) {
                        resolve(data)
                    } else if (headers['content-type'].match('xml')) {
                        try {
                            let jsonData = XMLParser.parse(data)
                            resolve(jsonData)
                        } catch (error) {
                            reject(new Error('Unable to parse XML data'))
                        }
                    } else {
                        reject(new Error('Unknown content type'))
                    }
                }).catch((err) => {
                    reject(new Error('Service unavailable', err))
                })
        });        
    }
}

module.exports = HTTPClient