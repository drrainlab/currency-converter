class Source {

    /**
     * Source class constructor
     * @param {String} name - exchange source name
     * @param {String} apiURL - exchange source API address
     * @param {String} cronUpdatePattern - exchange source update cron pattern
     * @param {String} baseCurrency - exchange source base currency
     * @param {String} adapterName - adapter name for parsing source rates
     * @param {Boolean} useByDefault - default source flag
     * @param {HTTPClient} client - HTTP client
     */

    constructor ({ name, apiURL, cronUpdatePattern, baseCurrency, useByDefault, adapterName, client }) {
        this.name = name
        this.adapterName = adapterName
        this.useByDefault = useByDefault
        this.baseCurrency = baseCurrency
        this.cronUpdatePattern = cronUpdatePattern
        this.apiURL = apiURL
        this.client = client
    }

    /**
     * Fetch exchange rates from current source
     * @param {Object} params - GET request params, {} by default
     * @returns {Object} Promise with resolved JSON representation of rates
     */  

    fetchRates (params = {}) {
        return this.client.get(this.apiURL, params)
    }
}

module.exports = Source