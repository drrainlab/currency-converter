const SourceAdapter = require('../SourceAdapter')

class CBRAdapter extends SourceAdapter {

    constructor (rates, baseCurrency) {
        super(rates, baseCurrency)
    }

    validate () {
        // todo
    }

    parse () {
        try {
            let ratesHash = {}        
            this.rates.ValCurs.Valute.forEach((currency) => {
                ratesHash[currency.CharCode] = parseFloat(currency.Value) / parseInt(currency.Nominal)
            })
            let ratio = 1 / ratesHash[this.serviceBaseCurrency]            
            for (let rate in ratesHash) ratesHash[rate] = (ratesHash[rate] * ratio).toFixed(4)
            ratesHash[this.baseCurrency] = ratio.toFixed(4)  
            this.normalizedRates = ratesHash      
            return ratesHash            
        } catch (error) {
            console.error('CBRAdapter: parsing error');            
        }
    }

}

module.exports = CBRAdapter