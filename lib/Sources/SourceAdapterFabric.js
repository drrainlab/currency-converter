class SourceAdapterFabric {
    
    constructor (adapterName) {
        this.adapterName = adapterName
    }

    create (rates, baseCurrency) {
        let Adapter = require(`./adapters/${this.adapterName}.adapter`)
        return new Adapter(rates, baseCurrency)
    }

}

module.exports = SourceAdapterFabric