class SourceAdapter {

    /**
     * SourceAdapter constructor
     * @param {Object} jsonRates - JSON rates from source
     * @returns new SourceAdapter instance
     */    

    constructor (jsonRates, baseCurrency = null) {
        this.rates = jsonRates
        this.baseCurrency = baseCurrency
        this.serviceBaseCurrency = process.env.BASE_CURRENCY || 'USD'
        this.parsedRates = null
    }

    /**
     * Normalized rates setter
     * @param {Object} parsedHash - normalized JSON rates
     */        

    set normalizedRates (parsedHash) {
        for (let rate in parsedHash) parsedHash[rate] = Number(parsedHash[rate])
        this.parsedRates = parsedHash
    }

    /**
     * @returns normalized JSON rates
     */   

    get normalizedRates () {
        return this.parsedRates && {
            sourceBaseCurrency: this.baseCurrency,
            serviceBaseCurrency: this.serviceBaseCurrency,
            rates: this.parsedRates
        }
    }

    validate () {

    }

    parse () {

    }

}

module.exports = SourceAdapter