"use strict";

const DbService = require("../../mixins/db.mixin");
const CacheCleaner = require("../../mixins/cache.cleaner.mixin")

module.exports = {
	name: "rates",
    mixins: [
			DbService("rates"),
			CacheCleaner
    ],
	/**
	 * Service settings
	 */
	settings: {
	},
	/**
	 * Actions
	 */
	actions: {

		/**
		 * Update source rates
		 *
		 * @returns source rates
		 */
		update: {
			params: {
				sourceId: "string",
				rates: "object"
			},
			async handler(ctx) {
				let { sourceId, rates } = ctx.params
				let sourceRates = await this.adapter.findOne({ sourceId })
				if (sourceRates) {
					return await this.adapter.updateById(sourceRates._id, { $set: { sourceId, rates }})				
				} else {
					return await this.adapter.insert({ sourceId, rates })				
				}
			}
		},
		/**
		 * Get source rates list
		 *
		 * @returns source rates
		 */
		list: {
			params: {
				sourceId: "string",
			},
			async handler(ctx) {
					let { rates } = await this.adapter.findOne({ sourceId: ctx.params.sourceId })
					return rates
			}
		},
		/**
		 * Get source currencies list
		 *
		 * @returns source rates
		 */
		currencies: {
			params: {
				sourceId: "string",
			},
			async handler(ctx) {
					let { rates } = await this.adapter.findOne({ sourceId: ctx.params.sourceId })
					return rates && Object.keys(rates)
			}
		},		
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {    
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {    
	},
	
	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};