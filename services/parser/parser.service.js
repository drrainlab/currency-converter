"use strict";

const cron = require('node-cron')
// const DbService = require("../../mixins/db.mixin");
const BullService = require("moleculer-bull");
const Source = require('../../lib/Source')
const HTTPClient = require('../../lib/HTTPClient')
const SourceAdapterFabric = require('../../lib/Sources/SourceAdapterFabric')

/*

use moleculer-cron and bull
run collect rates action 10 times a day (from all sources in db) 
each source parse action is a high priority job - resolving to save rates in db
when rates are ready call rates service to insertMany rates
*/


module.exports = {
	name: "parser",
    mixins: [
        // DbService("rates"),
        BullService(process.env.REDIS_URL),
	],
	dependencies: [
		"sources",
		"rates"
	],	
    queues: {
        "parse.source": {
            concurrency: 10,
            process (job) {			
				const source = new Source({...job.data.source, client: new HTTPClient()})
				const adapter = new SourceAdapterFabric(source.adapterName)
				const sourceId = job.data.sourceId
				console.log(`Parsing job received for ${source.name}, executing...`)
				return source.fetchRates()
					.then(rates => {						
						const currentSourceAdapter = adapter.create(rates, source.baseCurrency)
						// currentSourceAdapter.validate()
						currentSourceAdapter.parse()

						return this.broker.call('rates.update', {
							sourceId,
							rates: currentSourceAdapter.normalizedRates.rates
						})
					})
					.then(updatedRates => {
						return this.Promise.resolve({
							done: true,
							id: job.data.id,
							worker: process.pid
						});							
					})
					.catch((err) => {
						console.error(err);						
						return this.Promise.resolve({
							done: false,
							id: job.data.id,
							worker: process.pid
						});			  
					})          
			}
        },       
    }, 
	/**
	 * Service settings
	 */
	settings: {

	},
	/**
	 * Actions
	 */
	actions: {

		parseSources: {
			handler (ctx) {
				return this.broker.call('sources.list')
					.then(sources => sources.map(source => {
						// collect data immidiately
						this.createJob("parse.source", { source, sourceId: source._id })
						// then schedule a job
						console.log(`Scheduling job for ${source.name} with ${source.cronUpdatePattern} pattern`);
						return cron.schedule(source.cronUpdatePattern, () => {
							console.log(`Starting rates parsing job for ${source.name}`);
							this.createJob("parse.source", { source, sourceId: source._id })
						}).start()
					}))						
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {    
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

		this.actions.parseSources().then(scheduledTasks => {
			this.logger.info("Parsing tasks scheduled: ", scheduledTasks.length)
		})

		// let job = this.getJob("ParseExchangeSources")
        // if (!job.lastDate()) {
        //     job.start();
        // } else {
        //     console.log("ParseExchangeSources is already started!");
        // }            
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {		
	}
};