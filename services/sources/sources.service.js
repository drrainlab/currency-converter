"use strict";

const DbService = require("../../mixins/db.mixin");
const CacheCleaner = require("../../mixins/cache.cleaner.mixin")

module.exports = {
	name: "sources",
    mixins: [
		DbService("sources"),
		CacheCleaner
    ],
	/**
	 * Service settings
	 */
	settings: {
	},
	/**
	 * Actions
	 */
	actions: {

		/**
		 * Get sources list
		 *
		 * @returns sources array
		 */
		list: {
			async handler(ctx) {
				return await this.adapter.find()
			}
		},
		defaultSource: {
			async handler(ctx) {
				return await this.adapter.findOne({ useByDefault: true })
			}			
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		seedDB() {
			this.logger.info("Seed Sources Collection...");
			// Create sources
			return Promise.resolve()
				.then(() => this.adapter.insert({
					name: 'Central Bank of Russia', 
					apiURL: 'http://www.cbr.ru/scripts/XML_daily.asp',
					cronUpdatePattern: '0 0-23/2 * * *',
					baseCurrency: 'RUB',
					useByDefault: true, 
					adapterName: 'cbr'
				}))
				.then(sources => {
					this.logger.info(`Generated ${Array.isArray(sources) ? sources.length : 1} sources!`);
					this.clearCache();
				});
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {    
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {    
	},
	/**
	 * Service connected to MongoDB
	 */	
	afterConnected() {
		this.logger.info('Connected to Database...check if need seeding...')		
		return this.adapter.count().then(count => {
			if (count == 0) {
				this.logger.info('Ok, seeding with sources...')		
				this.seedDB();
			}
		});
	},
	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};