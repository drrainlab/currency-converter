"use strict";

module.exports = {
	name: "converter",

	/**
	 * Service settings
	 */
	settings: {

	},

	/**
	 * Service dependencies
	 */
	dependencies: [],	

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Convert from source to destination currency with optionally selected source 
		 * when no source selected - using default
		 * @param {String} from - source currency char code
		 * @param {String} to - destination currency char code
		 * @param {String} sourceId - exchanging source id
		 * @returns currencies rates
		 */
		rates: {
			params: {
				from: {
					type: "string",
					required: true,
					min: 2,
					max: 4
				},
				to: {
					type: "string",
					optional: true,
					min: 2,
					max: 4				
				},
				sourceId: {
					type: "string",
					optional: true
				}
			},
			handler (ctx) {		
				let { from, to, sourceId } = ctx.params
				from = String.prototype.toUpperCase.apply(from)
				let rates, fromRate, toRate
				return new Promise(async (resolve, reject) => {
					if (!sourceId) {
						let defaultSource = await this.broker.call('sources.defaultSource')
						sourceId = defaultSource._id
					}
					rates = await this.broker.call('rates.list', { sourceId })
					if (!rates) return reject(new Error("No rates found for selected source."))
					if (to) {
						to = String.prototype.toUpperCase.apply(to)
						return resolve({
							base: from,
							rates: [
								{ [to]: rates[from] / rates[to] }
							]
						})
					} else {
						return resolve({
							base: from,
							rates: Object.keys(rates).map(charCode => {
								return {
									[charCode]: rates[from] / rates[charCode]
								}
							})
						})						
					}
				})
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};