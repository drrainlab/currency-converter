FROM node:8-alpine

RUN mkdir /app
WORKDIR /app

ENV NODE_ENV=development

COPY package.json .

# RUN npm install --development

COPY . .

CMD ["npm", "run", "dev"]